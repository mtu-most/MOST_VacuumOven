/*  VacuumOven controls a vacuum oven designed to dry materials.
      This is firmware for a device to dry materials on a consumer level,
      primarily geared toward makers and small-scale plastics recyclers.
      It serves to track and control temperature of the vacuum chamber.

      The device was designed by researchers ing Michigan Technological
      University's MOST group <https://www.appropedia.org/Category:MOST>

      REVISIONS:
        v1.0.0 : First release, implements a single heater controller with
            a serial temperature output.

    Copyright (C) 2021 MOST
      ! Note that external libraries included with this software are
        subject to their own licenses, included within their respective
        folders.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


// Headers including a variety of definitions for the scale. These are separated
// to help reduce the length of this script.
#include "src\NTC_Thermistor\src\NTC_Thermistor.h"
#include "src\DataFilter.h"
#include "src\ArrayHelper.h"


// Size of averaging queues.
const uint8_t SAMPLE_SIZE = 10;

// Thermistor
// Analog Input pin.
const uint8_t ADC_PIN = A7;
// Nominal resistance, Ohms.
const uint32_t NOM_RES = 100000;
// Temperature for nominal resistance, deg C.
const uint8_t NOM_TEMP = 25;
// Beta Coefficient.
const uint16_t B_COEFF = 3950;
// Reference resistance (voltage divider), Ohms.
const uint32_t SER_RES = 29440;
// Thermistor Object.
NTC_Thermistor tSense(ADC_PIN, SER_RES, NOM_RES, NOM_TEMP, B_COEFF);
DataFilter tempQueue(SAMPLE_SIZE);

// Temperature control.
// Pin controlling heater relay.
const uint8_t HEATER_PIN = 10;
// Set temperature, *C.
uint8_t T_set = 80;
// Assume the thermistor is busted/disconnected if below this temp.
const uint8_t T_ERR = 0;

// Time tracker.
unsigned long lastRefresh;
unsigned long currentMillis;
// Refresh rate, ms.
const uint16_t reportPeriod  = 1000;

// Display precision.
const uint8_t PRECISION = 2;
const uint8_t VALUE_WIDTH = 10;


void setup() {
  Serial.begin(9600);

  while(!Serial){
    // Waiting for serial.
  } // Serial initialized.

  // AnalogRead compares to the voltage provided to the thermistor.
  analogReference(EXTERNAL);

  // Set up relay.
  pinMode(HEATER_PIN, OUTPUT);
  digitalWrite(HEATER_PIN, 0);

  // Empty the queues.
  tempQueue.fill(0);

  printHeader();
  lastRefresh=millis();
}


void loop() {
  // Read the temperature.
  tempQueue.push(tSense.readCelsius());

  // Make sure the temperature makes sense.
  checkThermistor();

  // Controller.
  double temp = tempQueue.getAverage();
  double tErr = temp - T_set;
  // On if below max temperature, off if at or above.
  bool heaterState = tErr >= 0 ? 0 : 1;

  digitalWrite(HEATER_PIN, heaterState);

  // Enforce report rate without hampering sample rate (sample rate
  // depends on how much processing is done between each call.
  currentMillis = millis();

  if (currentMillis - lastRefresh >= reportPeriod) {
    // Reset the time.
    lastRefresh += reportPeriod;

    printData();
  }
}


void printHeader() {
  String out;
  out += "\n\r";
  out += ArrayHelper::rightJustify("Time[ms]", VALUE_WIDTH);
  out += ",";
  out += ArrayHelper::rightJustify("T0[*C]", VALUE_WIDTH);
  out += ",";
  Serial.println(out);
  Serial.flush();
}


void checkThermistor() {
  // Assume that this will be used above freezing.
  bool isError = tempQueue.getAverage() < T_ERR;

  if (!isError) {
    return;
  }

  // Tell the user and shut 'er down.
  digitalWrite(HEATER_PIN, 0);
  Serial.println(F("Thermistor seems to  be disconnected"));

  while (isError) {
    // Wait for the thermistor to be re-connected.
    // Read the temperature.
    tempQueue.push(tSense.readCelsius());

    // Re-check.
    isError = tempQueue.getAverage() < T_ERR;
  } // Thermistor appears to be working.
}


void printData() {
  String out;
  out += ArrayHelper::rightJustify(String(currentMillis), VALUE_WIDTH);
  out += ",";
  out += ArrayHelper::rightJustify(
    String(tempQueue.getAverage(), PRECISION), VALUE_WIDTH);
  out += ",";

  Serial.println(out);
  Serial.flush();
}
