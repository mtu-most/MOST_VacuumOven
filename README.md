# VacuumOven

Vacuum drying can dehydrate materials further than dry heat methods while
protecting sensitive materials from thermal degradation. Many industries have
shifted to vacuum drying as cost- or time-saving measures. Small-scale vacuum
drying, however, has been limited by high costs of specialty scientific tools.
To make vacuum drying more accessible, the Michigan Tech Open Sustainability
Technology Laboratory ([MOST](https://www.appropedia.org/MOST)) has provided
design and performance information for a small-scale open source vacuum oven,
which can be fabricated from off-the-shelf and 3-D printed components. The oven
is tested for drying speed and effectiveness on both waste plastic polyethylene
terephthalate (PET) and a consortium of bacteria developed for bioprocessing of
terephthalate wastes to assist in distributed recycling of PET for both
additive manufacturing as well as potential food. Both of these materials can
be damaged when exposed to high temperatures, making vacuum drying a desirable
solution. The results showed the open source vacuum oven was effective at
drying both plastic and biomaterials, drying at a higher rate than a hot-air
dryer for small samples or for low volumes of water. The system can be
constructed for less than 20% of the cost of commercial vacuum dryers for
several laboratory-scale applications including dehydration of bio-organisms,
drying plastic for DRAM, and chemical processing.

This library defines firmware for the open source vacuum oven. The rest of the
design, as well as links to other information, are available on the Open
Science Framework ([OSF](https://osf.io/vf2b8/)).

## Features

The firmware is fairly lightweight - it manages a relay which controls a
heating element on the oven. Temperature is tracked using a thermistor. This
could be extended to handle multiple heaters or sensors. The temperature is
hardcoded into the firmware in order to allow plug-and-play functionality. A
user interface could be built on top of this firmware without issue.

The oven outputs timestamped temperature information for the purposes of data
logging and performance tracking.

## Installation Instructions

Clone this repository using `git clone
git@gitlab.com:mtu-most/MOST_VacuumOven.git`. This firmware depends on
[NTC_Thermistor](https://github.com/YuriiSalimov/NTC_Thermistor) by Salimov.
Install that library using the Arduino library manager or with
recurse-submodules in git.

Set the temperature on line 62, then flash this firmware to an Arduino (the
design instructions use a Nano v3.0). Follow the instructions described in the
journal article linked on OSF.
